# Апгрейд всех DECT-баз в рамках одной dect-системы
## Инструкция ниже приведена для запуска скрипта на Linux

### Клонируем проект
`git clone https://gitlab.com/d_nikolaev/upgrade_bases`  

### Переходим в созданный каталог, создаём виртуальное окружение
`cd upgrade_bases`  
`python3 -m venv venv`  

### Запускаем виртуальное окружение и устанавливаем библиотеки из файла requirements.txt
`source venv/bin/activate`  
`pip install -r requirements.txt`  

### Делаем скрипт исполняемым
`chmod +x main.py`  

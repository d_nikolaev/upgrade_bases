#!./venv/bin/python
# -*- coding: utf-8 -*

"""
Скрипт запускает апгрейд всей dect-системы
"""
import requests
import time
import ipaddress
import random
from bs4 import BeautifulSoup  # для распарсивания ответов на request-запросы
from urllib3 import disable_warnings, exceptions  # для отключения сообщений Warning о небезопасности сертификата

disable_warnings(exceptions.InsecureRequestWarning)  # Отключаем предупреждения Warning о небезопасности сертификата

def check_ip(ip_address):
    """Функция проверки корректности ввода ip-адреса"""
    try:
        ipaddress.ip_network(ip_address)
        return True
    except:
        return False


def inputIP():
    """Запрашиваем IP-адрес DECT-базы у пользователя, который был получен после её первичного включения"""
    while True:
        IP = input("Введите IP-адрес DECT-базы master: ")
        if check_ip(IP):
            return IP
        else:
            print("Введен некорректный IP-адрес")


def session(ip, login, pwd):
    """Фунция инициализации сессии для базы с 10 релизом"""
    s = requests.Session()
    data = {'file': 'admin.xml?xsl=admin.xsl&sys=on', 'usr': login, 'pwd': pwd, 'login': 'Login'}
    url = f"https://{ip}/login-req.xml"
    r = s.post(url, data=data, verify=False)
    # s.cookies
    html = r.text
    soup = BeautifulSoup(html, 'lxml')
    tag = soup.find('login-res')
    result = tag.get('result')
    sessions_user = tag.get('sessions-user')
    sessions_total = tag.get('sessions-total')
    error = tag.get('error')
    if result == "ok":
        return s
    else:
        print("Неудачно")
        print(f"Error: {error}")
        print(f"Login: {result}")
        exit()

def main():
    """Функция main, выполняет все действия программы, описанные в ней"""
    ## Вводим переменные
    ip = inputIP() # Запрашиваем у пользователя IP master-базы 
    login = "admin"
    pwd = "<PASSWORD>"

    ## Запрашиваем список всех dect-баз в системе
    list_bases = []
    link = f'https://{ip}/GW-DECT/MASTER/mod_cmd.xml?cmd=xml-radios&xsl=asc_dectmaster_radios.xsl'
    r = requests.get(link, auth=(login, pwd), verify=False)
    html = r.text
    soup = BeautifulSoup(html, 'lxml')
    list_bases = []
    for tag in soup.find_all("radio"):
        addr = tag.get('addr')
        if addr != None:
            list_bases.append(addr)
    print("Всего DECT-баз: ", len(list_bases))

    ## Запускаем апгрейд slave-баз
    print('АПГРЕЙД SLAVE-БАЗ')
    for base in list_bases:
        if base != ip and base != '127.0.0.1':
            print(f'-----------{base}---------------')
            print('Запуск апгрейда базы до релиза 10.2.9')
            print('Прошивка bootcode.....................', end=' ')
            url = f'https://{base}/FLASHMAN0/post_file.xml?xsl=up_firmware_resp.xsl&dest=boot'
            files = {'IPBS2_boot_v10p2p9.bin': open('IPBS2_boot_v10p2p9.bin', 'rb')}
            r = requests.post(url, auth=(login, pwd), files=files, verify=False)
            print('done')
            print("Прошивка firmware.....................", end=' ')
            time.sleep(2)
            url = f'https://{base}/FLASHMAN0/post_file.xml?xsl=up_firmware_resp.xsl'
            files = {'IPBS2_firm_v10p2p9.bin': open('IPBS2_firm_v10p2p9.bin', 'rb')}
            r = requests.post(url, auth=(login, pwd), files=files, verify=False)
            print('done')
            print("Перезагрузка базы.....................", end=' ')
            time.sleep(2)
            link = f'https://{base}/CMD0/mod_cmd.xml?cmd=ireset&xsl=reset.xsl&ireset=OK'
            r = requests.get(link, auth=(login, pwd), verify=False)
            time.sleep(15)
            print('done')
    
    ## Запускаем апгрейд мастер-базы
    print('АПГРЕЙД MASTER-БАЗЫ')
    print(f'-----------{ip}---------------')
    print('Запуск апгрейда базы до релиза 10.2.9')
    print('Прошивка bootcode.....................', end=' ')
    url = f'https://{ip}/FLASHMAN0/post_file.xml?xsl=up_firmware_resp.xsl&dest=boot'
    files = {'IPBS2_boot_v10p2p9.bin': open('IPBS2_boot_v10p2p9.bin', 'rb')}
    r = requests.post(url, auth=(login, pwd), files=files, verify=False)
    print('done')
    print("Прошивка firmware.....................", end=' ')
    time.sleep(2)
    url = f'https://{ip}/FLASHMAN0/post_file.xml?xsl=up_firmware_resp.xsl'
    files = {'IPBS2_firm_v10p2p9.bin': open('IPBS2_firm_v10p2p9.bin', 'rb')}
    r = requests.post(url, auth=(login, pwd), files=files, verify=False)
    print('done')
    print("Перезагрузка базы.....................", end=' ')
    time.sleep(2)
    link = f'https://{ip}/CMD0/mod_cmd.xml?cmd=ireset&xsl=reset.xsl&ireset=OK'
    r = requests.get(link, auth=(login, pwd), verify=False)
    time.sleep(15)
    print('done')


if __name__ == '__main__':
    main()